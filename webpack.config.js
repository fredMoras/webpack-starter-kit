const webpack = require("webpack");
var path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: "./src/index.js",
    // output: {
    //     path: path.resolve(__dirname, "./public"),
    //     filename: 'js/app.js',
    // },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },

      {
        test: /\.(css|sass|scss)$/,
        use: [
            MiniCssExtractPlugin.loader,
            {
                loader: 'css-loader',
                options: {
                    importLoaders: 2,
                    sourceMap: true
                }
            },
            {
                loader: 'postcss-loader',
                options: {
                    plugins: () => [
                        require('autoprefixer')
                    ],
                    sourceMap: true
                }
            },
            {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
            }
        ]
      },

    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '../css/[name].css',

    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ],
    devServer: {
        // Pour afficher les erreurs sur la page
        overlay: true,
        open:true,
        // Si on veut rediriger un chemin vers une API ou autre
        proxy: {
            "/web": {
                target: "http://localhost:8000",
                pathRewrite: {"^/web" : ""}
            }
        },
        // Fichiers à distribuer
        contentBase: path.resolve('./public'),
        // CORS
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },
    devtool: "eval-source-map"

};
